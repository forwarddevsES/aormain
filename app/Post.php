<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Actuallymab\LaravelComment\Contracts\Commentable;
use Actuallymab\LaravelComment\HasComments;
use Cog\Contracts\Love\Likeable\Models\Likeable as LikeableContract;
use Cog\Laravel\Love\Likeable\Models\Traits\Likeable;
use Spatie\Activitylog\Traits\LogsActivity;

class Post extends Model implements Commentable, LikeableContract
{
    use HasComments, Likeable, LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content', 'title', 'thumb', 'slug', 'commentable'];

    protected static $logAttributes = ['title'];

    protected static $logName = 'posts';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function setCommentableAttribute($value)
    {
        $this->attributes['commentable'] = boolval($value);
    }

    public function canComment()
    {
        return $this->attributes['commentable'];
    }

}
