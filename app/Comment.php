<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Actuallymab\LaravelComment\Contracts\Commentable;
use Actuallymab\LaravelComment\HasComments;
use Cog\Contracts\Love\Likeable\Models\Likeable as LikeableContract;
use Cog\Laravel\Love\Likeable\Models\Traits\Likeable;
use Actuallymab\LaravelComment\Models\Comment as CommentBase;

class Comment extends CommentBase implements LikeableContract
{
    use Likeable;
}