<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\UpdatePostRequest;
use Illuminate\Support\Str;
use App\Post;
use App\Comment;

class PostController extends Controller
{   

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $user = request()->user();
        $validated = $request->validated();

        $name = kebab_case($request->title);
        $extension = $request->thumb->extension();
        $nameImage = time() . "{$name}.$extension";
        $validated['thumb'] = $nameImage;
        $validated['slug'] = Str::slug($request->title);

        $request->thumb->storeAs('posts', $nameImage);

        $post = $user->posts()->create($validated);
        return redirect('/')
                ->withSuccess('Entrada publicada correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('dashboard.blog.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        $validated = $request->validated();
        $post = Post::findOrFail($id);

        if($request->hasFile('thumb')) {
            $name = kebab_case($request->title);
            $extension = $request->thumb->extension();
            $nameImage = time() . "{$name}.$extension";
            $validated['thumb'] = $nameImage;
            $validated['slug'] = Str::slug($request->title);
            $request->thumb->storeAs('posts', $nameImage);
            $post = $user->posts()->create($validated);
        }
        $post->update($validated);
        return redirect('/')
                ->withSuccess('Entrada actualizada correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return redirect('/')
            ->withSuccess('Entrada eliminada correctamente.');
    }

    public function likesCount($id) {
        $post = Post::findOrFail($id);
        return [
            'likes' => $post->likesCount,
            'liked' => $post->liked
        ];
    }

    /**
     * Toggle like 
     *
     * @param [type] $id
     * @return void
     */
    public function toggleLike($id) {
        $post = Post::findOrFail($id);
        $post->toggleLikeBy();
        return [
            'likes' => $post->likesCount,
            'liked' => $post->isLikedBy(),
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function comments($postId) {
        $post = Post::findOrFail($postId);
        return $post->comments()
            ->withCount(['likes'])
            ->with('commented:id,name,avatar')
            ->paginate(10);
    }

    public function storeComment($postId, CommentRequest $request) {
        $user = $request->user();
        $post = Post::findOrFail($postId);
        $user->comment($post, $request->text);
        return ['success' => true];
    }

    public function commentLikesCount($id) {
        $comment = Comment::findOrFail($id);
        return [
            'likes' => $comment->likesCount,
            'liked' => $comment->liked
        ];
    }

    /**
     * Toggle like 
     *
     * @param [type] $id
     * @return void
     */
    public function commentToggleLike($id) {
        $comment = Comment::findOrFail($id);
        $comment->toggleLikeBy();
        return [
            'likes' => $comment->likesCount,
            'liked' => $comment->isLikedBy(),
        ];
    }


    public function destroyComment($postId, $commentId) {
        $user = request()->user();
        $comment = $user->comments()->findOrFail($commentId);
        return [
            'success' => $comment->delete()
        ];
    }

}
