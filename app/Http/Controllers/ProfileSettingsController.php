<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use Illuminate\Support\Str;
use Storage;
use Spatie\Activitylog\Models\Activity;

class ProfileSettingsController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user = Auth::user();
        return view('dashboard.profile.settings.index', compact('user'));
    }

    public function changePassword(Request $request) {
        if (!(Hash::check($request->get('password'), Auth::user()->password))) {
            return redirect()->back()->with("error","Su contraseña actual no coincide con la contraseña que usted proporcionó. Por favor, inténtalo de nuevo");
        }

        if(strcmp($request->get('password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","La nueva contraseña no puede ser la misma que la contraseña actual. Por favor, elija una contraseña diferente.");
        }
        $validated = $request->validate([
            'password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        $user = Auth::user();
        $request->user()->fill([
            'password' => Hash::make($request->get('new_password'))
        ])->save();
        return redirect()->back()->with("success","Contraseña cambiada con éxito!");
    }

    public function uploadProfilePicture(Request $request) {
        $validated = $request->validate([
            'avatar' => 'required|image',
        ]);
        
        $user = $request->user();
        if ($user->avatar != null && Storage::exists('profiles/' . $user->avatar)) {
            Storage::delete('profiles/' . $user->avatar);
        }
        $name = Str::random(4);
        $extension = $request->avatar->extension();
        $nameImage = time() . "{$name}.$extension";
        $validated['avatar'] = $nameImage;

        $request->avatar->storeAs('profiles', $nameImage);
        $user->update($validated);
        return redirect()
                ->back()
                ->withSuccess('Foto de perfil actualizada correctamente.');
    }

    public function showLoginLog() {
        $activities = Activity::inLog('auth.login')
            ->causedBy(Auth::user())->latest()->paginate(15);
        return view('dashboard.profile.settings.logins')->with(compact('activities'));
    }
}
