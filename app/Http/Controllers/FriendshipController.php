<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Notifications\FriendshipRequest;

class FriendshipController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function send($id) {
        $to = User::findOrFail($id);
        $user = Auth::user();
        if ($user->hasSentFriendRequestTo($to) || $user->isFriendWith($to)) {
            return redirect('/')
                ->withError('Ya le enviaste una solicitud de amistad, espera que te responda.');    
        }

        $user->befriend($to);
        $to->notify(new FriendshipRequest($user));
        return redirect('/')
                ->withSuccess('Solicitud de amistad enviada');
    }

    public function accept($id, Request $request) {
        $request->validate([
            'notiId' => 'required|string'
        ]);
        $sender = User::findOrFail($id);
        $user = Auth::user();
        $user->acceptFriendRequest($sender);
        $noti = $user->unreadNotifications()->findOrFail($request->notiId);
        $noti->markAsRead();
        return ['success' => 'Solicitud de amistad aceptada'];
    }

    public function deny($id, Request $request) {
        $request->validate([
            'notiId' => 'required|string'
        ]);
        $sender = User::findOrFail($id);
        $user = Auth::user();
        $user->denyFriendRequest($sender);
        $noti = $user->unreadNotifications()->findOrFail($request->notiId);
        $noti->markAsRead();
        return ['success' => 'Peticion de amistad eliminada correctamente'];
    }
}
