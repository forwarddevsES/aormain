<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Actuallymab\LaravelComment\CanComment;
use Cog\Contracts\Love\Liker\Models\Liker as LikerContract;
use Cog\Laravel\Love\Liker\Models\Traits\Liker;
use Hootlex\Friendships\Traits\Friendable;

class User extends Authenticatable implements LikerContract
{
    use Notifiable, CanComment, Liker, Friendable;

    protected $appends = ['profile_picture'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function getIsAdminAttribute()
    {
        return true;
    }
    public function getProfilePictureAttribute($value) {
        if($this->attributes['avatar'] != null) {
            return asset('storage/profiles/'. $this->attributes['avatar']);
        }
        return asset('avatar.png');
    }
}
