<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('skin');
            $table->integer('dinero');
            $table->integer('nivel')->default(1);
			$table->float('PosX', 5, 5);
			$table->float('PosY', 5, 5);
			$table->float('PosZ', 5, 5);
			$table->float('Vida', 4, 3);
			$table->float('Chaleco', 4, 3);
			$table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
}
