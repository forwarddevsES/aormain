<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'dashboard.index');
// Auth routes
Auth::routes(['verify' => true]);
Route::get('register', function () {
    return redirect('login#register');
})->name('register');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout');

// Blog
Route::prefix('blog')->group(function () {
    Route::resource('posts', 'PostController')->except(['destroy']);
    Route::get('posts/{id}/destroy', 'PostController@destroy')->name('posts.destroy');
    Route::get('posts/{id}/togglelike', 'PostController@toggleLike')->name('posts.togglelike');
    Route::get('posts/{id}/likescount', 'PostController@likesCount');
    Route::get('posts/{id}/comments', 'PostController@comments');
    Route::post('posts/{id}/comments', 'PostController@storeComment');
    Route::get('posts/{idp}/comments/{idc}/destroy', 'PostController@destroyComment');
    Route::get('comments/{id}/togglelike', 'PostController@commentToggleLike');
    Route::get('comments/{id}/likescount', 'PostController@commentLikesCount');
});

// Personajes
Route::get('/personaje', function () {
    return view('dashboard.character');
});
Route::get('/añadir-personaje', 'RegisterCharacterController@view')->name('add-character');

// Profile settings
Route::get('profile/settings', 'ProfileSettingsController@index')->name('profile.settings.index');
Route::view('profile/settings/changepassword', 'dashboard.profile.settings.changepassword')
->name('profile.settings.changepassword')->middleware('auth');
Route::post('profile/settings/changepassword', 'ProfileSettingsController@changePassword')->name('profile.settings.changepassword');
Route::post('profile/settings/uploadphoto', 'ProfileSettingsController@uploadProfilePicture')->name('profile.settings.uploadphoto');
Route::get('profile/settings/logins', 'ProfileSettingsController@showLoginLog')->name('profile.settings.logins');

// Friendships
Route::get('friendships/{id}/send', 'FriendshipController@send');
Route::post('friendships/{id}/accept', 'FriendshipController@accept');
Route::post('friendships/{id}/deny', 'FriendshipController@deny');