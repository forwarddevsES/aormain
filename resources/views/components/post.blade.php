<div class="modal fade" id="popnotice-{{$post->id}}">

    <div class="modal-dialog ui-block window-popup blog-post-popup">
        <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
            <svg class="olymp-close-icon">
                <use xlink:href="{{asset('rainbow/icons/icons.svg')}}#olymp-close-icon"></use>
            </svg>
        </a>
        <article class="hentry post has-post-thumbnail thumb-full-width" style="width: 100%;">

            <div class="post__author author vcard inline-items">
                {{-- <img src="{{asset($post->user->avatar)}}" alt="author"> --}}

                <div class="author-date">
                    <a class="h6 post__author-name fn" href="02-ProfilePage.html">{{$post->user->name}}</a> escribió
                    una <a href="#">publicación</a>
                    <div class="post__date">
                        <time class="published" datetime="2017-03-24T18:18">
                            {{Carbon\Carbon::createFromTimeStamp(strtotime($post->created_at))->diffForHumans()}}
                        </time>
                    </div>
                </div>

                <div class="more"><svg class="olymp-three-dots-icon">
                        <use xlink:href="{{asset('rainbow/icons/icons.svg')}}#olymp-three-dots-icon"></use>
                    </svg>
                    <ul class="more-dropdown">
                        <li>
                            <a href="{{ route('posts.edit', $post->id) }}">Editar Post</a>
                        </li>
                        <li>
                            <a href="{{ route('posts.destroy', $post->id) }}">Borrar Post</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="post-thumb">
                <img src="{{asset('storage/posts/' . $post->thumb)}}" alt="photo">
                <a href="#" class="h1 post-title">{{$post->title}}</a>
                <div class="overlay"></div>
            </div>


            {!! Parsedown::instance()->text($post->content) !!}

            <div class="post-additional-info inline-items" style="margin-bottom: 5px;">

                {{-- <a href="#" class="post-add-icon inline-items">
                    <svg class="olymp-heart-icon">
                        <use xlink:href="{{asset('rainbow/icons/icons.svg')}}#olymp-heart-icon"></use>
                    </svg>
                    <span>{{$post->likesCount}}</span>
                </a> --}}
            <likes :id="{{$post->id}}" type="posts"></likes>

                <div class="comments-shared">
                    <a href="#" class="post-add-icon inline-items">
                        <svg class="olymp-speech-balloon-icon">
                            <use xlink:href="{{asset('rainbow/icons/icons.svg')}}#olymp-speech-balloon-icon"></use>
                        </svg>
                        <span>{{ $post->totalCommentsCount() }}</span>
                    </a>

                </div>


            </div>

            <div class="control-block-button post-control-button">

                <a href="#" class="btn btn-control">
                    <svg class="olymp-like-post-icon">
                        <use xlink:href="{{asset('rainbow/icons/icons.svg')}}#olymp-like-post-icon"></use>
                    </svg>
                </a>

                <a href="#" class="btn btn-control">
                    <svg class="olymp-comments-post-icon">
                        <use xlink:href="{{asset('rainbow/icons/icons.svg')}}#olymp-comments-post-icon"></use>
                    </svg>
                </a>

                <a href="#" class="btn btn-control">
                    <svg class="olymp-share-icon">
                        <use xlink:href="{{asset('rainbow/icons/icons.svg')}}#olymp-share-icon"></use>
                    </svg>
                </a>

            </div>
            @auth
        <comments :id="{{ $post->id}}"> </comments>
        @endauth
        
        </article>

        <div class="mCustomScrollbar" data-mcs-theme="dark">

            

        </div>
    </div>
</div>
