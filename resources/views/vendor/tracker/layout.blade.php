@extends('layouts.main')

@section('content')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

    {{-- <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.css" rel="stylesheet"> --}}
    <div id="wrapper">
        <div class="container">
            <div class="row">
                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                    <ul class="cat-list-bg-style align-center ">
                        <li class="cat-list__item {{ Session::get('tracker.stats.page') =='visits' ? 'active' : '' }}"><a href="{{route('tracker.stats.index')}}?page=visits" class="">@lang("tracker::tracker.visits")</a></li>
                        <li class="cat-list__item {{ Session::get('tracker.stats.page') =='summary' ? 'active' : '' }}"><a href="{{route('tracker.stats.index')}}?page=summary">@lang("tracker::tracker.summary") </a></li>
                        <li class="cat-list__item {{ Session::get('tracker.stats.page') =='users' ? 'active' : '' }}"><a href="{{route('tracker.stats.index')}}?page=users" class="">@lang("tracker::tracker.users")</a></li>
                        <li class="cat-list__item {{ Session::get('tracker.stats.page') =='events' ? 'active' : '' }}"><a href="{{route('tracker.stats.index')}}?page=events" class="">@lang("tracker::tracker.events")</a></li>

                    </ul>
                </div>
                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                    
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h2 class="">{{$title}}</h2>
                            <div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
                                <ul class="more-dropdown">
                                    <li><a href="{{route('tracker.stats.index')}}?days=0">@lang("tracker::tracker.today")</a></li>
                                    <li><a href="{{route('tracker.stats.index')}}?days=1">@choice("tracker::tracker.no_days",1, ["count" => 1])</a></li>
                                    <li><a href="{{route('tracker.stats.index')}}?days=7">@choice("tracker::tracker.no_days",7, ["count" => 7])</a></li>
                                    <li><a href="{{route('tracker.stats.index')}}?days=30">@choice("tracker::tracker.no_days",30, ["count" => 30])</a></li>
                                    <li><a href="{{route('tracker.stats.index')}}?days=365">@choice("tracker::tracker.no_years",1, ["count" => 1])</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="ui-block-content">
                            @yield('page-contents')
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            @yield('page-secondary-contents')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

@stop
@push('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.7.0/moment.min.js"></script>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js
"></script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        let tb = document.getElementById('table_div');
        tb.classList.add('table', 'table-striped');
    });
</script>
@endpush