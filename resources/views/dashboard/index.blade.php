@extends('layouts.main')
@section('styles')
<style media="screen">
  .carousel-inner > .item > img, .carousel-inner > .item > a > img {
  display: block;
  height: 400px;
  min-width: 100%;
  width: 100%;
  max-width: 100%;
  line-height: 1;
}
</style>
@endsection
@section('content')
  
  
  @include('widgets.carousel')


  <div class="container" style="padding-top: 40px;">
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="page-description">
                    <div class="icon">
                        <svg class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="FAV PAGE"><use xlink:href="#olymp-star-icon"></use></svg>
                    </div>
                    <span>Tu cuenta no esta vinculada con un personaje. Click <a href="{{ route('add-character') }}">aqui</a> para vincular</span>
                </div>
        </div>
    </div>
    <div class="row">
      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-8">
        @include('widgets.posts')
      </div>
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">
        {{-- @include('widgets.lastshout') --}}
        @include('widgets.users')
        {{-- @include('widgets.services') --}}
      </div>
    </div>
  </div>


@endsection

@section('script')
  
  <script type="text/javascript">
  $('#myCarousel').carousel();
var winWidth = $(window).innerWidth();
$(window).resize(function () {

  if ($(window).innerWidth() < winWidth) {
      $('.carousel-inner>.item>img').css({
          'min-width': winWidth, 'width': winWidth
      });
  }
  else {
      winWidth = $(window).innerWidth();
      $('.carousel-inner>.item>img').css({
          'min-width': '', 'width': ''
      });
  }
});
  </script>

@endsection
