@extends('layouts.main')
@section('content')
<div class="container" style="padding-top: 40px;">
    <div class="row">

        <div class="col col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="ui-block-title bg-blue">
                    <h6 class="c-white">Crear una nueva entrada en el blog</h6>
                </div>
                <div class="ui-block-content">
                    <form action="{{ route('posts.update', $post->id) }}" method="POST" enctype="multipart/form-data" class="row">
                        @csrf
                        @method('PUT')
                        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Titulo</label>
                                <input required name="title" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                            type="text" placeholder="" value="{{ $post->title }}">
                                <span class="material-input"></span>
                                @if ($errors->has('title'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Contenido</label>
                                <textarea required name="content" id="body" class="form-control {{ $errors->has('content') ? 'is-invalid' : '' }}"
                                    style="height: 240px">{{ $post->content }}</textarea>
                                <span class="material-input"></span>

                                <small id="passwordHelpBlock" class="form-text text-muted">
                                    Recuerda que puedes usar <a target="_blank" href="https://www.markdownguide.org/cheat-sheet/">Markdown</a>
                                </small>
                                @if ($errors->has('content'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                            <p>Activar comentarios</p>
                            <div class="togglebutton">
                                <label>
                                    <input name="commentable" type="checkbox" {{ $post->commentable ? 'checked' : '' }} value="true"><span class="toggle"></span>
                                </label>
                            </div>
                            @if ($errors->has('commentable'))
                            <span class="">
                                <strong>{{ $errors->first('commentable') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="file-upload">
                                <label for="upload" class="file-upload__label">Subir imagen</label>
                                <input accept="image/*" id="upload" class="file-upload__input" type="file" name="thumb">
                            </div>
                            @if ($errors->has('thumb'))
                                <span class="">
                                    <strong>{{ $errors->first('thumb') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <button type="button" href="#" onclick="preview(event)" class="btn btn-lg full-width">Previsualizar<div
                                    class="ripple-container">
                                    <div class="ripple ripple-on" style="left: 99.25px; top: 39.25px; background-color: rgb(255, 255, 255); transform: scale(45.2812);"></div>
                                </div></button>
                        </div>
                        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <button type="submit" class="btn btn-blue btn-lg full-width">Publicar<div class="ripple-container">
                                    <div class="ripple ripple-on" style="left: 99.25px; top: 39.25px; background-color: rgb(255, 255, 255); transform: scale(45.2812);"></div>
                                </div></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
            <div class="ui-block" id="post-preview" style="display: none;">
                <div id="preview" class="ui-block-content"></div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/1.9.0/showdown.min.js"></script>

<script>
    function preview() {
        document.getElementById('post-preview').style.display = 'block';
        let text = document.getElementById('body').value;
        let converter = new showdown.Converter();
        let html = converter.makeHtml(text);
        document.getElementById('preview').innerHTML = html;
    }

</script>
@endpush
