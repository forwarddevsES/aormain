@extends('layouts.settings')
@section('content2')
<div class="ui-block">
    <div class="ui-block-title">
        <h6 class="title">Mis ingresos de sesion</h6>
    </div>
    <div class="ui-block-content">
        <table class="table">
            <thead>
                <tr>
                    <th>IP</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                @foreach($activities as $activity)
                <tr>
                    <td>{{ $activity->getExtraProperty('ip') }}</td>
                    <td>{{ $activity->created_at->diffForHumans() }} </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        {{ $activities->links() }}

    </div>
</div>
@endsection