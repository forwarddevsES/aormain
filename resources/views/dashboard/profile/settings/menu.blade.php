<div class="ui-block">

				
				
    <!-- Your Profile  -->
    
    <div class="your-profile">
        <div class="ui-block-title ui-block-title-small">
            <h6 class="title">Tu PERFIL</h6>
        </div>
    
        <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" role="tab" id="headingOne">
                    <h6 class="mb-0">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Ajustes del perfil
                            <svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon') }}"></use></svg>
                        </a>
                    </h6>
                </div>
    
                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                    <ul class="your-profile-menu">
                        <li>
                        <a href="{{ route('profile.settings.index') }}">Información personal</a>
                        </li>
                        
                        <li>
                        <a href="{{ route('profile.settings.changepassword')}}">Cambiar contraseña</a>
                        </li>

                        <li>
                        <a onclick="$('#update-header-photo').modal()" href="javascript:void(0)">Subir foto de perfil</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="accordion2" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <div class="card-header" role="tab" id="headingOne">
                        <h6 class="mb-0">
                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                Mis actividades
                                <svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon') }}"></use></svg>
                            </a>
                        </h6>
                    </div>
        
                    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <ul class="your-profile-menu">
                            <li>
                            <a href="{{ route('profile.settings.logins') }}">Ingresos</a>
                            </li>
                            
                            <li>
                            <a href="#">Otros</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        
        
    </div>
    
    <!-- ... end Your Profile  -->
    

</div>