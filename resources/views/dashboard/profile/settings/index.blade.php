@extends('layouts.settings')
@section('content2')
    <div class="ui-block">
        <div class="ui-block-title">
            <h6 class="title">Informacion personal</h6>
        </div>
        <div class="ui-block-content">
            <div class="row">
                <div class="col col-md-6 col-xs-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Nombre</label>
                    <input class="form-control" placeholder="" disabled type="text" value="{{ $user->name }}">
                    <span class="material-input"></span></div>
                </div>
                <div class="col col-md-6 col-xs-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Rango</label>
                    <input class="form-control" placeholder="" type="text" disabled value="">
                    <span class="material-input"></span></div>
                </div>
                <div class="col col-md-6 col-xs-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Contraseña</label>
                    <input class="form-control" placeholder="" type="text" disabled value="*******">
                    <span class="material-input"></span></div>
                </div>
                <div class="col col-md-6 col-xs-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Correo electrónico</label>
                    <input class="form-control" placeholder="" type="text" disabled value="{{ $user->email }}">
                    <span class="material-input"></span></div>
                </div>
                <div class="col col-md-6 col-xs-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Fecha de registro</label>
                    <input class="form-control" placeholder="" type="text" disabled value="{{ $user->created_at->format('Y-m-d')}}">
                    <span class="input-group-addon">
                        <svg class="olymp-month-calendar-icon icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-month-calendar-icon"></use></svg>
                    </span>
                    <span class="material-input"></span></div>
                </div>
            </div>
        </div>
    </div>
@endsection