@extends('layouts.settings')

@section('content2')
<div class="ui-block">
    <div class="ui-block-title">
        <h6 class="title">Cambiar contraseña</h6>
    </div>
    <div class="ui-block-content">
    <form action="{{ route('profile.settings.changepassword') }}" method="POST" class="row">
            @csrf
            <div class="col col-md-12 col-xs-12">
                <div class="form-group label-floating">
                    <label class="control-label">Contraseña actual</label>
                <input required name="password" class="form-control" placeholder="" type="password">
                <span class="material-input"></span></div>
            </div>
            <div class="col col-md-6 col-xs-12">
                <div class="form-group label-floating">
                    <label class="control-label">Nueva contraseña</label>
                <input required name="new_password" class="form-control" placeholder="" type="password">
                <span class="material-input"></span></div>
            </div>
            <div class="col col-md-6 col-xs-12">
                <div class="form-group label-floating">
                    <label class="control-label">Repetir nueva contraseña</label>
                <input required name="new_password_confirmation" class="form-control" placeholder="" type="password">
                <span class="material-input"></span></div>
            </div>

            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <button class="btn btn-primary btn-lg full-width">Cambiar contraseña</button>
            </div>
        </form>
    </div>
</div>

@endsection