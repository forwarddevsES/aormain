@extends('layouts.main')

@section('pcustyles')
<link rel="stylesheet" type="text/css" href="{{asset('css/character.css')}}">
@endsection

@section('content')
  <div class="container" style="padding-top: 40px;">
    <div class="row">
			<div class="col-12">
        		<div class="ui-block">
					<div class="ui-block-title">
						<h6 class="title">Estadisticas del personaje</h6>
					</div>
					<div class="ui-block-content d-flex">
						<div class="pr-3">
							<img src="https://static.up-cdn.com/roleplay/mapa/images/skins/0.jpg" alt="skin 0">
							<div class="progress h-pro" style="border-radius: 0;">
								<div class="progress-bar bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>
							</div>
							<div class="progress" style="border-radius: 0;">
								<div class="progress-bar ar-pro" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>
							</div>
    				</div>
						<div>
							<p><strong>Nombre_Apellido:</strong> Joe_Kovacs</p>
							<p><strong>Nivel:</strong> 1</p>
							<p><strong>Dinero:</strong> $15.000</p>
							<p><strong>Edad:</strong> 19</p>
						</div>
					</div>
					<div class="ui-block-title">
						<h6 class="title">Inventario</h6>
					</div>
					<div class="ui-block-content d-flex">
						<div class="pr-3">
							<table class="inventario-table">
								<tbody>
									<tr>
										<td><strong>Experiencia: </strong></td>
										<td>52/60</td>
									</tr>
									<tr>
										<td><strong>Dinero banco: </strong></td>
										<td>$150.000</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection