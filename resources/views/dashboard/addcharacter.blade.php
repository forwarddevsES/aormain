@extends('layouts.main')

@section('pcustyles')
<link rel="stylesheet" type="text/css" href="{{asset('css/character.css')}}">
@endsection

@section('content')
	<div class="container" style="padding-top: 40px;">
		<div class="row">
			<div class="col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Añadir personaje</h6>
				</div>
				<div class="ui-block-content">
					<form action="{{ route('add-character') }}" method="POST" accept-charset="utf-8">
						<form-addcharacter></form-addcharacter>
					</form>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
