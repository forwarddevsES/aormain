<!DOCTYPE html>
<html lang="es">
<head>

	<title>Rainbow - SAMP Server</title>

	<!-- Required meta tags always come first -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Main Font -->
	<script src="{{asset('js/webfontloader.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>

	<!-- Bootstrap CSS -->
	{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> --}}
	<link rel="stylesheet" href="/Bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-select.css') }}">


	<!-- Theme Styles CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('css/theme-styles.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/blocks.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/fonts.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
	<!-- Styles for plugins -->
	<link rel="stylesheet" type="text/css" href="{{asset('css/simplecalendar.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" />
	<link rel="stylesheet" type="text/css" href="{{asset('css/daterangepicker.css')}}">

	<!-- Lightbox popup script-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
	
	<!--PCU STYLES-->
	@yield('pcustyles')
	@yield('styles')

</head>
<body>
<div id="app">
@include('layouts.ext.sidebar')



<!-- include('layouts.ext.chat') -->


<!-- Start navbar  -->
@auth
  @include('layouts.ext.headerauth')
@else
  @include('layouts.ext.header-dark')
@endauth

<!-- Start content  -->
<div>
		@if (session('success'))
		<div class="container">
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 40px">
						<span>{{ session('success') }}</span>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
						</button>
				</div>
		</div>
    
		@endif
		@if (session('error'))
		<div class="container">
				<div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top: 40px">
					<span>{{ session('error') }}</span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
				</div>
		</div>
    
    @endif
	@yield('content')
</div>
<!-- End content -->


<!-- Window-popup Update Header Photo -->

<div class="modal fade" id="update-header-photo">
	<div class="modal-dialog ui-block window-popup update-header-photo">
		<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
			<svg class="olymp-close-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-close-icon"></use></svg>
		</a>

		<div class="ui-block-title">
			<h6 class="title">Actualizar foto de perfil</h6>
		</div>

		<a onclick="document.getElementById('upload-profilephoto').click()" href="javascript:void(0)" class="upload-photo-item">
			<svg class="olymp-computer-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-computer-icon"></use></svg>

			<h6>Subir foto</h6>
			<span>Busca en tu ordenador.</span>
		</a>
	<form id="form-uploadphoto" action="{{ route('profile.settings.uploadphoto')}}" method="post" enctype="multipart/form-data" style="display: none;">
		@csrf
		<input accept="image/*" name="avatar" type="file" id="upload-profilephoto" onchange="document.getElementById('form-uploadphoto').submit()">
		</form>

	</div>
</div>


<!-- ... end Window-popup Update Header Photo -->

<!-- Window-popup-CHAT for responsive min-width: 768px -->

<div class="ui-block popup-chat popup-chat-responsive">
	<div class="ui-block-title">
		<span class="icon-status online"></span>
		<h6 class="title" >Chat</h6>
		<div class="more">
			<svg class="olymp-three-dots-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-three-dots-icon"></use></svg>
			<svg class="olymp-little-delete js-chat-open"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-little-delete"></use></svg>
		</div>
	</div>
	<div class="mCustomScrollbar">
		<ul class="notification-list chat-message chat-message-field">
			<li>
				<div class="author-thumb">
					<img src="{{asset('img/avatar14-sm.jpg')}}" alt="author" class="mCS_img_loaded">
				</div>
				<div class="notification-event">
					<span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
					<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
				</div>
			</li>

			<li>
				<div class="author-thumb">
					<img src="{{asset('img/author-page.jpg')}}" alt="author" class="mCS_img_loaded">
				</div>
				<div class="notification-event">
					<span class="chat-message-item">Don’t worry Mathilda!</span>
					<span class="chat-message-item">I already bought everything</span>
					<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:29pm</time></span>
				</div>
			</li>

			<li>
				<div class="author-thumb">
					<img src="{{asset('img/avatar14-sm.jpg')}}" alt="author" class="mCS_img_loaded">
				</div>
				<div class="notification-event">
					<span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
					<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
				</div>
			</li>
		</ul>
	</div>

	<form>

		<div class="form-group label-floating is-empty">
			<label class="control-label">Press enter to post...</label>
			<textarea class="form-control" placeholder=""></textarea>
			<div class="add-options-message">
				<a href="#" class="options-message">
					<svg class="olymp-computer-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-computer-icon"></use></svg>
				</a>
				<div class="options-message smile-block">

					<svg class="olymp-happy-sticker-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-happy-sticker-icon"></use></svg>

					<ul class="more-dropdown more-with-triangle triangle-bottom-right">
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat1.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat2.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat3.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat4.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat5.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat6.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat7.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat8.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat9.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat10.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat11.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat12.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat13.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat14.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat15.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat16.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat17.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat18.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat19.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat20.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat21.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat22.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat23.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat24.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat25.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat26.png')}}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{asset('img/icon-chat27.png')}}" alt="icon">
							</a>
						</li>
					</ul>
				</div>
			</div>
			 </div>

	</form>


</div>
</div>
<!--JQUERY-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<!--POPPER-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<!--JS PROCESADO-->
<script src="{{ asset('js/app.js') }}"></script>

<!--PLUGINS-->
<script src="/js/jquery.appear.js"></script>
<script src="/js/jquery.mousewheel.js"></script>
{{-- <script src="/js/perfect-scrollbar.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js" integrity="sha256-pOydVY7re8c1n+fEgg3uoslR/di9NMsOFXJ0Esf2xjQ=" crossorigin="anonymous"></script>
<script src="/js/jquery.matchHeight.js"></script>
<script src="/js/svgxuse.js"></script>
<script src="/js/imagesloaded.pkgd.js"></script>
<script src="/js/Headroom.js"></script>
<script src="/js/velocity.js"></script>
<script src="/js/ScrollMagic.js"></script>
<script src="/js/jquery.waypoints.js"></script>
<script src="/js/jquery.countTo.js"></script>


<!-- Js effects for material design. + Tooltips -->
<script src="{{asset('js/material.min.js')}}"></script>

<!-- Helper scripts (Tabs, Equal height, Scrollbar, etc) -->
{{-- <script src="{{asset('js/theme-plugins.js')}}"></script> --}}
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

<!-- Load more news AJAX script -->
<script src="{{asset('js/ajax-pagination.js')}}"></script>

<!-- Select / Sorting script -->
<script src="{{asset('js/selectize.min.js')}}"></script>

<!-- Datepicker input field script-->
<script src="{{asset('js/moment.min.js')}}"></script>
<script src="{{asset('js/daterangepicker.min.js')}}"></script>

<!-- Widget with events script-->
<script src="{{asset('js/simplecalendar.js')}}"></script>

<!-- Lightbox plugin script-->
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>


<script src="{{asset('js/mediaelement-and-player.min.js')}}"></script>
<script src="{{asset('js/mediaelement-playlist-plugin.min.js')}}"></script>

<script defer src="{{asset('fonts/fontawesome-all.js')}}"></script>


@yield('script')
@stack('script')

@yield('required-scripts-bottom')

    <script>
	    @yield('inline-javascript')
    </script>
<script src="{{asset('js/base-init.js')}}"></script>

<!--BOOTSTRAP-->
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> --}}
<script src="{{asset('Bootstrap/dist/js/bootstrap.bundle.js')}}"></script>
</body>
</html>
