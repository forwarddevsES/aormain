<div class="header--standard header--standard-dark" id="header--standard">
	<div class="container">
    <div class="header--standard-wrap">
      <a href="{{url('/')}}" class="logo">
        <div class="img-wrap">
          <img src="{{asset('rainbow/img/logo-colored-small.png')}}" alt="Rainbow">
        </div>

      </a>
			<a href="{{url('/')}}" class="logo">

				<div class="title-block">
					<h6 class="logo-title">rainbow</h6>
					<div class="sub-title">CONTROL PANEL</div>
				</div>
			</a>

			<a href="#" class="open-responsive-menu js-open-responsive-menu">
				<svg class="olymp-menu-icon"><use xlink:href="{{asset('rainbow/icons/icons.svg')}}#olymp-menu-icon"></use></svg>
			</a>

			<div class="nav nav-pills nav1 header-menu">
				<div class="mCustomScrollbar ps ps--theme_default ps--active-y" data-ps-id="ea313c9f-379c-f2bf-cfc9-b960558c1186">
					<ul>
            <li class="nav-item">
              <a href="{{url('/')}}" class="nav-link">Inicio</a>
            </li>

						<li class="nav-item">
							<a href="#" class="nav-link">Foro</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link">Servicios</a>
						</li>
            <li class="nav-item">
              <a href="{{route('login')}}" class="btn btn-primary" style="padding: 10px 10px 10px 10px; margin-bottom: 0px;">Ingreso<div class="ripple-container"></div></a>
            </li>
						&nbsp;&nbsp;
						<li class="nav-item">
              <a href="{{route('login')}}" class="btn btn-primary" style="padding: 10px 10px 10px 10px; margin-bottom: 0px;">Registro<div class="ripple-container"></div></a>
            </li>
						<li class="close-responsive-menu js-close-responsive-menu">
							<svg class="olymp-close-icon"><use xlink:href="{{asset('rainbow/icons/icons.svg')}}#olymp-close-icon"></use></svg>
						</li>
					</ul>
				<div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__scrollbar-y-rail" style="top: 0px; height: 73px; right: 0px;"><div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 30px;"></div></div></div>
			</div>
		</div>
	</div>
</div>
