@extends('layouts.main')

@section('content')
    <div class="container" style="margin-top: 40px">
        <div class="row">
            <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
                @yield('content2')
            </div>
            <div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">
                @include('dashboard.profile.settings.menu')
            </div>

        </div>
    </div>
@endsection