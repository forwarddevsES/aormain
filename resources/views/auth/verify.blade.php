@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="block-ui">
                <div class="block-ui-title">Verifica tu correo electrónico</div>

                <div class="block-ui-content">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.
                        </div>
                    @endif

                    Antes de continuar, por favor revise su correo electrónico para ver si hay un enlace de verificación.
                    Si no recibió el correo electrónico, <a href="{{ route('verification.resend') }}">haga clic aquí para solicitar otro</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
