@extends('layouts.main')

@section('content')
  <div class="container">
  	<div class="row display-flex">
  		<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
  			<div class="landing-content">
  				<h1>Welcome to the Biggest Social Network in the World</h1>
  				<p>We are the best and biggest social network with 5 billion active users all around the world. Share you
  					thoughts, write blog posts, show your favourite music via Stopify, earn badges and much more!
  				</p>
  				<a href="#" class="btn btn-md btn-border c-white">Register Now!</a>
  			</div>
  		</div>

  		<div class="col col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">

  			<!-- Login-Registration Form  -->

  			<div id="registration" class="registration-login-form">
  				<!-- Nav tabs -->
  				<ul class="nav nav-tabs" role="tablist">
  					<li class="nav-item">
  						<a class="nav-link active" data-toggle="tab" href="#login" role="tab">
  							<svg class="olymp-login-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-login-icon"></use></svg>
  						</a>
  					</li>
  					<li class="nav-item">
  						<a id="registration-link" class="nav-link" data-toggle="tab" href="#register" role="tab">
  							<svg class="olymp-register-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-register-icon"></use></svg>
  						</a>
  					</li>
  				</ul>

  				<!-- Tab panes -->
  				<div class="tab-content">
            <div class="tab-pane active" id="login" role="tabpanel" data-mh="log-tab">
  						<div class="title h6">Login</div>
  						<form class="content" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
  							<div class="row">
  								<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
  									<div class="form-group label-floating is-empty">
                                        <label class="control-label">Email</label>
  										<input class="form-control" type="email" name="login_email" value="{{ old('login_email') }}">
                      @if ($errors->has('login_email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('login_email') }}</strong>
                          </span>
                      @endif
  									</div>
  									<div class="form-group label-floating is-empty">
                                        <label class="control-label">Contraseña</label>
  										<input class="form-control" id="password" type="password" name="login_password">
                      @if ($errors->has('login_password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('login_password') }}</strong>
                          </span>
                      @endif
  									</div>

  									<div class="remember">

  										<div class="checkbox">
  											<label>
  												<input name="optionsCheckboxes" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
  												Recordarme
  											</label>
  										</div>
  										<a href="#" class="forgot">Olvide mi Contraseña</a>
  									</div>

  									<button type="submit" class="btn btn-lg btn-primary full-width">Ingresar</button>

  									<div class="or"></div>

  									<!--<a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left"><i class="fab fa-facebook-f" aria-hidden="true"></i>Login with Facebook</a>

  									<a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left"><i class="fab fa-twitter" aria-hidden="true"></i>Login with Twitter</a>-->


  									<p>¿Todavía no estas registrado? <a  onclick="$('#registration-link').tab('show')" href="#register">¡Registrate!</a></p>
  								</div>
  							</div>
  						</form>
  					</div>
  					<div class="tab-pane" id="register" role="tabpanel" data-mh="log-tab">
  						<div class="title h6">Registrate</div>
  						<form class="content" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
  							<div class="row">
  								<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
  									<div class="form-group label-floating is-empty">
  										<label class="control-label">Nombre de Usuario</label>
  										<input class="form-control" placeholder="" type="text" name="name" value="{{ old('name') }}" >
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
  									</div>
  								</div>
  								<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
  									<div class="form-group label-floating is-empty">
  										<label class="control-label">Email</label>
  										<input class="form-control" type="email" name="email" value="{{ old('email') }}" required>
                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
  									</div>
  								</div>
  								<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
  									<div class="form-group label-floating is-empty">
  										<label class="control-label">Contraseña</label>
                      <input id="password" type="password" class="form-control" name="password" required>
                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
  									</div>
  									<div class="form-group label-floating is-empty">
  										<label class="control-label">Repite tu contraseña</label>
  										  <input id="password-confirm" type="password" class="form-control" placeholder="" name="password_confirmation" required>
  									</div>



  									<div class="remember">
  										<div class="checkbox">
  											<label>
  												<input name="optionsCheckboxes" type="checkbox">
  												Acepto los terminos y condiciones
  											</label>
  										</div>
  									</div>

  									<button type="submit" class="btn btn-purple btn-lg full-width">Registrarse</button>
  								</div>
  							</div>
  						</form>
  					</div>
  				</div>
  			</div>

  			<!-- ... end Login-Registration Form  -->		</div>
  	</div>
  </div>
@endsection



@section('script')
<script>
    let anchor = window.location.hash
    if (anchor == '#register') {
        $('#registration-link').tab('show')
    }
</script>
@endsection