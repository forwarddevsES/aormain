<div class="ui-block">
  <div class="news-feed-form">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active inline-items" data-toggle="tab" href="#home-1" role="tab" aria-expanded="true">

          <svg class="olymp-status-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-status-icon"></use></svg>

          <span>Historia</span>
        </a>
      </li>


      <li class="nav-item">
        <a class="nav-link inline-items" data-toggle="tab" href="#blog" role="tab" aria-expanded="false">
          <svg class="olymp-blog-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-blog-icon"></use></svg>

          <span>Shout</span>
        </a>
      </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">
        <form method="post" action="{{route('postfeed')}}">
          {{ csrf_field() }}
          {{ method_field('POST') }}
          <div class="author-thumb">
            <img src="{{asset(Auth::user()->avatar)}}" style="width:36px;" alt="author">
          </div>
          <div class="form-group with-icon label-floating is-empty">
            <label class="control-label">Escribe lo que estás pensando...</label>
            <textarea class="form-control" placeholder="" name="feed_text"></textarea>
          <span class="material-input"></span></div>
          <div class="add-options-message">
            <a href="#" class="options-message" data-toggle="tooltip" data-placement="top" data-original-title="ADD PHOTOS">
              <svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-camera-icon"></use></svg>
            </a>

            <button class="btn btn-primary btn-md-2">Publicar</button>


          </div>

        </form>
      </div>



      <div class="tab-pane" id="blog" role="tabpanel" aria-expanded="true">
        <form>
          <div class="author-thumb">
            <img src="{{asset(Auth::user()->avatar)}}" style="width:36px;" alt="author">
          </div>
          <div class="form-group with-icon label-floating is-empty">
            <label class="control-label">Comparte con toda la comunidad algo breve...</label>
            <textarea class="form-control" placeholder=""></textarea>
          <span class="material-input"></span></div>
          <div class="add-options-message">
            <a href="#" class="options-message" data-toggle="tooltip" data-placement="top" data-original-title="ADD PHOTOS">
              <svg class="olymp-camera-icon" data-toggle="modal" data-target="{{asset('icons/icons.svg')}}#update-header-photo"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-camera-icon"></use></svg>
            </a>


            <button class="btn btn-primary btn-md-2">Publicar</button>

          </div>

        </form>
      </div>
    </div>
  </div>
</div>
