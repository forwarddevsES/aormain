
  <div class="ui-block">
    <article class="hentry blog-post single-post single-post-v2">

      <div class="control-block-button post-control-button">
        <a href="#" class="btn btn-control has-i bg-facebook">
          <i class="fa fa-facebook" aria-hidden="true"></i>
        </a>

        <a href="#" class="btn btn-control has-i bg-twitter">
          <i class="fa fa-twitter" aria-hidden="true"></i>
        </a>
      </div>

      <a href="#" class="post-category bg-transparent">Ultima noticia</a>
      <h1 class="post-title">{{$news->title}}</h1>
      <div class="post-thumb">
        <img src="{{asset($news->thumb)}}" alt="author">
      </div>
      <div class="single-post-additional inline-items">
        <div class="post__author author vcard inline-items">
          <img alt="author" src="{{asset($news->user->avatar)}}" class="avatar">
          <div class="author-date not-uppercase">
            <a class="h6 post__author-name fn" href="#">{{$news->user->name}}</a>
            <div class="author_prof">
              Autor
            </div>
          </div>
        </div>
        <div class="post-date-wrap inline-items">
          <svg class="olymp-calendar-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-calendar-icon"></use></svg>
          <div class="post-date">
            <a class="h6 date" href="#">{{Carbon::createFromTimeStamp(strtotime($news->created_at))->diffForHumans()}}</a>
            <span>Fecha</span>
          </div>
        </div>
        <div class="post-comments-wrap inline-items">
          <svg class="olymp-comments-post-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-comments-post-icon"></use></svg>
          <div class="post-comments">
            <a class="h6 comments" href="#">{{$news->comments->count()}}</a>
            <span>Comentarios</span>
          </div>
        </div>
      </div>




      <div class="post-content-wrap">

        <div class="post-content">
          {!!$news->new!!}
        </div>
      </div>

    </article>

    <ul class="comments-list">
      @foreach ($news->comments as $comment)
        <li>
          <div class="post__author author vcard inline-items">
            <img src="{{asset($comment->user->avatar)}}" alt="author">

            <div class="author-date">
              <a class="h6 post__author-name fn" href="#">{{$comment->user->name}}</a>
              <div class="post__date">
                <time class="published" datetime="2004-07-24T18:18">
                  {{Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans()}}
                </time>
              </div>
            </div>

            <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-three-dots-icon"></use></svg></a>

          </div>

          <p>{{$comment->comment_text}}</p>

          <a href="#" class="post-add-icon inline-items">
            <svg class="olymp-heart-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-heart-icon"></use></svg>
            <span>{{$comment->likes->count()}}</span>
          </a>
          <a href="#" class="reply">Responder</a>
        </li>
      @endforeach

    </ul>

    <a href="#" class="more-comments">Ver más comentarios <span>+</span></a>
    @auth
      <div class="ui-block">
        <div class="news-feed-form">

          <div class="tab-content">
            <div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">
              <form method="post" action="{{route('commentnotice', ['id' => $news->id])}}">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="author-thumb">
                  <img src="{{asset(Auth::user()->avatar)}}"  style="width:36px;" alt="author">
                </div>
                <div class="form-group with-icon label-floating is-empty">
                  <label class="control-label">Escribe lo que quieras comentar en la noticia...</label>
                  <textarea class="form-control" placeholder="" name="comment_text"></textarea>
                  <span class="material-input"></span></div>
                  <div class="add-options-message">
                    <button type="submit" class="btn btn-primary btn-md-2">Comentar</button>
                  </div>

                </form>
              </div>

            </div>
          </div>
        </div>

      @endauth

    </div>
