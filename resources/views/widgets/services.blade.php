<div class="ui-block">
  <div class="ui-block-title">
    <h6 class="title">Estado de los servicios</h6>
    <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-three-dots-icon"></use></svg></a>
  </div>
  <div class="ui-block-content">
    <fieldset disabled="">
      <div class="form-group label-floating has-danger is-empty">
        <label class="control-label">Servidor</label>
        <input type="text" class="form-control form-control-error" placeholder="">
        <span class="material-input"></span>
      </div>
    </fieldset>
    <fieldset disabled="">
      <div class="form-group label-floating has-success is-empty">
        <label class="control-label">Web</label>
        <input type="text" class="form-control form-control-success" placeholder="">
        <span class="material-input"></span>
      </div>
    </fieldset>
  </div>
</div>
