<div class="row sorting-container" id="posts-grid-1" data-layout="masonry">
    @foreach (App\Post::latest()->get() as $post)
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 sorting-item">
        <div class="ui-block">
            <article class="hentry blog-post blog-post-v2">

                <div class="post-thumb">
                    <img src="{{asset('img/post1.jpg')}}" alt="photo">

                    <div class="post-content">
                        <a href="#" class="post-category bg-blue-light">SERVIDOR</a>
                        <a href="#" class="h4 post-title" data-toggle="modal" data-target="#popnotice-{{$post->id}}">{{$post->title}}</a>

                        <div class="author-date">
                            por
                            <a class="h6 post__author-name fn" href="#">{{$post->user->name}}</a>
                            <div class="post__date">
                                <time class="published" datetime="2017-03-24T18:18">
                                    -
                                    {{Carbon\Carbon::createFromTimeStamp(strtotime($post->created_at))->diffForHumans()}}
                                </time>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="post-additional-info inline-items">

                    <ul class="friends-harmonic">

                        <li>
                            <a href="#">
                                <img src="{{asset('img/icon-chat26.png')}}" alt="icon">
                            </a>
                        </li>
                    </ul>
                    <div class="names-people-likes">
                        {{ $post->likesCount }}
                    </div>

                    <div class="comments-shared">
                        <a href="#" class="post-add-icon inline-items">
                            <svg class="olymp-speech-balloon-icon">
                                <use xlink:href="{{asset('icons/icons.svg')}}#olymp-speech-balloon-icon"></use>
                            </svg>
                            <span>{{$post->totalCommentsCount()}}</span>
                        </a>
                    </div>

                </div>

            </article>
        </div>
    </div>
    {{-- @include('popups.notice', ['notice' => $post]) --}}
    @component('components.post', ['post' => $post])
    @endcomponent
    @endforeach
</div>
