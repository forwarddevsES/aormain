@php
  $shout = Shout::orderBy('id', 'desc')->first();
@endphp
<div class="ui-block">
  <div class="widget w-birthday-alert">

    <div class="content">
      <div class="author-thumb">
        <img src="{{asset($shout->user->avatar)}}" alt="author">
      </div>
      <span>{{Carbon::createFromTimeStamp(strtotime($shout->created_at))->diffForHumans()}}</span>
      <a href="#" class="h4 title">{{$shout->shout}}</a>
      @guest
        <p>¡Inicia sesión para publicar!</p>
      @endguest
    </div>
  </div>
</div>
