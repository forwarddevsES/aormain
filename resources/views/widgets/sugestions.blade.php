<div class="ui-block">
  <div class="ui-block-title">
    <h6 class="title">Sugerencias de amigos</h6>
    <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-three-dots-icon"></use></svg></a>
  </div>
  @php
    $lastusers = User::orderBy('id', 'desc')->take(3);
  @endphp
  <ul class="widget w-friend-pages-added notification-list friend-requests">
    @foreach ($lastusers as $lastuser)
      <li class="inline-items">
        <div class="author-thumb">
          <img src="{{asset($lastuser->avatar)}}" alt="author">
        </div>
        <div class="notification-event">
          <a href="#" class="h6 notification-friend">{{$lastuser->name}}</a>
          <span class="chat-message-item">1 Amigo en común</span>
        </div>
        <span class="notification-icon">
          <a href="#" class="accept-request">
            <span class="icon-add without-text">
              <svg class="olymp-happy-face-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-happy-face-icon"></use></svg>
            </span>
          </a>
        </span>

      </li>
    @endforeach




  </ul>

</div>
