<div id="newsfeed-items-grid">

  @foreach ($feeds as $feed)
    <div class="ui-block">
      <article class="hentry post video">

        <div class="post__author author vcard inline-items">
          <img src="{{asset($feed->user->avatar)}}" alt="author">

          <div class="author-date">
            @if ($feed->type == 1)
              <a class="h6 post__author-name fn" href="#">{{$feed->user->name}}</a> publicó una historia
            @elseif($feed->type == 3)
              <a class="h6 post__author-name fn" href="#">{{$feed->user->name}}</a> compartió un <a href="#">enlace</a>
            @endif
            <div class="post__date">
              <time class="published" datetime="2004-07-24T18:18">
                {{Carbon::createFromTimeStamp(strtotime($feed->created_at))->diffForHumans()}}
              </time>
            </div>
          </div>

          <div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-three-dots-icon"></use></svg>
            <ul class="more-dropdown">
              <li>
                <a href="#">Editar Post</a>
              </li>
              <li>
                <a href="#">Borrar Post</a>
              </li>
              <li>
                <a href="#">Turn Off Notifications</a>
              </li>
              <li>
                <a href="#">Select as Featured</a>
              </li>
            </ul>
          </div>

        </div>

        <p>{{$feed->feed_text}}</p>



        <div class="post-additional-info inline-items">

          <a href="#" class="post-add-icon inline-items">
            <svg class="olymp-heart-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-heart-icon"></use></svg>
            <span>{{$feed->likes->count()}}</span>
          </a>

          <ul class="friends-harmonic">
            @php
              $flikes = $feed->likes->take(5);
            @endphp
            @foreach ($flikes as $like)
              <li>
                <a href="#">
                  <img src="{{asset($like->user->avatar)}}" style="width:36px;" alt="friend">
                </a>
              </li>
            @endforeach
          </ul>
          @if ($feed->likes->count())
            <div class="names-people-likes">
              A @foreach ($flikes as $namelikes)
                <a href="#">{{$namelikes->user->name}}</a>
              @endforeach y a
              <br>{{$feed->likes->count()-2}} personas más les gustó esto.
            </div>
          @endif


          <div class="comments-shared">
            <a href="#" class="post-add-icon inline-items">
              <svg class="olymp-speech-balloon-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-speech-balloon-icon"></use></svg>

              <span>{{$feed->comments->count()}}</span>
            </a>

            <a href="#" class="post-add-icon inline-items">
              <svg class="olymp-share-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-share-icon"></use></svg>

              <span>16</span>
            </a>
          </div>


        </div>

        <div class="control-block-button post-control-button">

          <a href="#" class="btn btn-control">
            <svg class="olymp-like-post-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-like-post-icon"></use></svg>
          </a>

          <a href="#" class="btn btn-control">
            <svg class="olymp-comments-post-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-comments-post-icon"></use></svg>
          </a>

          <a href="#" class="btn btn-control">
            <svg class="olymp-share-icon"><use xlink:href="{{asset('icons/icons.svg')}}#olymp-share-icon"></use></svg>
          </a>

        </div>

      </article>
    </div>
  @endforeach


</div>
