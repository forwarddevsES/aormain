<div class="ui-block">
  <div class="ui-block-title">
    <h6 class="title">Usuarios conectados</h6>
  </div>
  <div class="ui-block-content">
    <ul class="widget w-faved-page">
      @foreach (Tracker::users(30) as $user)
      <li data-toggle="tooltip" title="{{ $user->user->name }}">
        <img src="{{ $user->user->profile_picture}}" style="width:36px; height: 36px;" alt="author">
      </li>
      @endforeach

      <li class="all-users">
        +{{Tracker::users(30)->count()}}
      </li>
    </ul>
  </div>
</div>

@push('script')
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
  });
</script>
@endpush
